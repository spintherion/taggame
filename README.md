# Tag-game (W.I.P.)

This is a project made by two college students, as a side-project to further their knowledge in several fields.

The basic concept of the game, is to have people find each other by looking on a list. They will then be able to choose someone, and from there, they will see the location of that specific person. The person they are looking for will receive a notification, and a QR code will appear on their screen. The person looking will be able to scan that QR code to get their points.
